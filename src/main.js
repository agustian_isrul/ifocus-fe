import { createApp } from 'vue'
import App from './App.vue'
import router from './services/router'
import { createPinia } from 'pinia'

import moment from 'moment-timezone';

import PrimeVue from 'primevue/config';
import StyleClass from 'primevue/styleclass';
import Ripple from 'primevue/ripple';
import Breadcrumb from 'primevue/breadcrumb';
import InputText from 'primevue/inputtext';
import Password from 'primevue/password';
import Message from 'primevue/message';
import Panel from 'primevue/panel';
import Dropdown from 'primevue/dropdown';
import Button from 'primevue/button';
import Toast from 'primevue/toast';
import Dialog from 'primevue/dialog';
import Calendar from 'primevue/calendar';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ConfirmDialog from 'primevue/confirmdialog';
import Textarea from 'primevue/textarea';
import FileUpload from 'primevue/fileupload';
import DataView from 'primevue/dataview';
import Tooltip from 'primevue/tooltip';
import RadioButton from 'primevue/radiobutton';
import Checkbox from 'primevue/checkbox';
import Card from 'primevue/card';
import PickList from 'primevue/picklist';
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import Menu from 'primevue/menu';
import PanelMenu from 'primevue/panelmenu';
import CustomPickList from './views/Directives/CustomPickList.vue';
import DynamicDialog from 'primevue/dynamicdialog';
import MultiSelect from 'primevue/multiselect';
import InputNumber from 'primevue/inputnumber';

import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';
import DialogService from 'primevue/dialogservice';

import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import "primeflex/primeflex.css";
import 'primeicons/primeicons.css';

const app = createApp(App)

moment.tz.setDefault('Asia/Jakarta');
Date.prototype.toJSON = function(){ return moment(this).format(); }

app.use(createPinia())
app.use(router)
app.use(PrimeVue, { ripple: true });
app.use(ToastService);
app.use(ConfirmationService);
app.use(DialogService);

app.directive("styleclass", StyleClass);
app.directive("ripple", Ripple);
app.directive('tooltip', Tooltip);

app.component("Breadcrumb", Breadcrumb);
app.component('InputText', InputText);
app.component("Password", Password);
app.component("Message", Message);
app.component("Panel", Panel);
app.component("Dropdown", Dropdown);
app.component("Button", Button);
app.component("Toast", Toast);
app.component("Dialog", Dialog);
app.component("Calendar", Calendar);
app.component("DataTable", DataTable);
app.component("Column", Column);
app.component("ConfirmDialog", ConfirmDialog);
app.component("Textarea", Textarea);
app.component("FileUpload", FileUpload);
app.component("DataView", DataView);
app.component("RadioButton", RadioButton);
app.component("Checkbox", Checkbox);
app.component("Card", Card);
app.component("PickList", PickList);
app.component("TabView", TabView);
app.component("TabPanel", TabPanel);
app.component("Menu", Menu);
app.component("PanelMenu", PanelMenu);
app.component("CustomPickList", CustomPickList);
app.component("DynamicDialog", DynamicDialog);
app.component("MultiSelect", MultiSelect);
app.component("InputNumber", InputNumber);

app.mount('#app')
