import axios from 'axios';

const axiosService = () => {
  // const defaultOptions = {
  //   headers: {
  //     "Content-type": "application/json",
  //   },
  // };

  // let instance = axios.create(defaultOptions);
  let instance = axios.create();

  instance.interceptors.request.use(function (request) {
    request.headers["Content-Type"] = request.headers["Content-Type"] ? request.headers["Content-Type"] : 'application/json';
    request.headers["Authorization"] = localStorage.getItem('tokenUser');
    return request;
  });

  return instance;
};

export default axiosService();