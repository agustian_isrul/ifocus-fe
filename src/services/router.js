import { createRouter, createWebHistory } from 'vue-router'
import MainPage from './../views/MainPage.vue'
import DashboardPage from './../views/Dashboard.vue';
import Upload from '../views/IncentiveScheme/Upload.vue';
import LoginPage from './../views/LoginPage.vue';
import CustomerPanel from './../views/CustomerOutlet/Panel.vue'
import CustomerPanelApproval from './../views/CustomerOutlet/PanelApproval.vue'
import CustomerPanelCutOff from './../views/CustomerOutlet/PanelCutOff.vue'
import CustomerPanelOverview from './../views/CustomerOutlet/PanelOverview.vue'
import CustomerPanelSpecial from './../views/CustomerOutlet/PanelSpecial.vue'
import NewClinicMaster from './../views/ClinicMaster/AddNew.vue'
import NewClinicMap from './../views/ClinicMap/AddNew.vue';
import EditClinicMap from './../views/ClinicMap/edit.vue';
import ViewClinicMap from './../views/ClinicMap/overview.vue';
import UploadClinicMap from './../views/ClinicMap/upload.vue';
import EditClinicMaster from './../views/ClinicMaster/EditClinic.vue'
import ApprovalClinicMaster from './../views/ClinicMaster/ApprovalClinic.vue'
import IncentiveConfig from './../views/IncentiveScheme/IncentiveConfig.vue'
import CreateIncentive from './../views/IncentiveScheme/CreateIncentive.vue'
import PreSimulation from './../views/IncentiveScheme/PreSimulation.vue'
import PreSimulationVictory from './../views/IncentiveScheme/PreSimulationVictory.vue'
import EditScheme from './../views/IncentiveScheme/EditScheme.vue'
import SalesReport from '../views/CustomerRegular/SalesReport.vue';
import AddCustomerRegular from './../views/CustomerRegular/AddCustomerRegular.vue'
import OverviewCustomerRegular from './../views/CustomerRegular/OverviewCustRegular.vue'
import UploadCustomerRegular from './../views/CustomerRegular/UploadCustRegular.vue'
import ClaimSalesPanel from './../views/Sales/ClaimSalesPanel.vue'
import AddContribution from './../views/CustomerContribution/AddContribution.vue'
import OverviewContribution from './../views/CustomerContribution/OverviewContribution.vue'
import UploadContribution from '../views/CustomerContribution/UploadContribution.vue';
import additionalDataUpload from '../views/DataPendukung/FileUpload.vue';

const tempHistory = process.env.NODE_ENV === 'production' ? createWebHistory() : createWebHistory('/dev');
const router = createRouter({
  history: tempHistory,
  routes: [
    {
      path: '', component: MainPage, redirect: '/dashboard',
      beforeEnter: (to, from, next) => {
        const tempLocalStorage = localStorage.getItem("tokenUser");
        if (!tempLocalStorage) {
          next({ name: 'login' })
        }
        else next()
      },
      children: [
        { name: 'dashboard', path: '/dashboard', component: DashboardPage },
        {
          name: 'customer-panel',
          path: '/customerpanel',
          component: CustomerPanel,
          meta: {
            breadcrumb: {
              name: 'Customer Outlet Panel'
            }
          }
        },
        {
          name: 'customer-panel-special',
          path: '/customerpanelspecial',
          component: CustomerPanelSpecial,
          meta: {
            breadcrumb: {
              name: 'Customer Outlet Panel Special'
            }
          }
        },
        {
          name: 'customer-panel-approval',
          path: '/customerpanelapproval',
          component: CustomerPanelApproval,
          meta: {
            breadcrumb: {
              name: 'Approval Customer Outlet Panel'
            }
          }
        },
        {
          name: 'customer-panel-cutoff',
          path: '/customerpanelcutoff',
          component: CustomerPanelCutOff,
          meta: {
            breadcrumb: {
              name: 'Cut Off Customer Outlet Panel'
            }
          }
        },
        {
          name: 'customer-panel-overview',
          path: '/customerpaneloverview',
          component: CustomerPanelOverview,
          meta: {
            breadcrumb: {
              name: 'Overview Customer Outlet Panel'
            }
          }
        },
        {
          name: 'clinic-master-new',
          path: '/newclinicmaster',
          component: NewClinicMaster,
          meta: {
            breadcrumb: {
              name: 'Add New Clinic Master'
            }
          }
        },
        {
          name: 'clinic-master-edit',
          path: '/editclinicmaster',
          component: EditClinicMaster,
          meta: {
            breadcrumb: {
              name: 'Edit New Clinic Master'
            }
          }
        },
        {
          name: 'clinic-master-approval',
          path: '/approvalclinicmaster',
          component: ApprovalClinicMaster,
          meta: {
            breadcrumb: {
              name: 'Approval Clinic Master'
            }
          }
        },
        {
          name: 'clinic-map-new',
          path: '/newclinicmap',
          component: NewClinicMap,
          meta: {
            breadcrumb: {
              name: 'Add Clinic Map'
            }
          }
        },
        {
          name: 'clinic-map-edit',
          path: '/editclinicmap',
          component: EditClinicMap,
          meta: {
            breadcrumb: {
              name: 'Open/Edit Clinic Map'
            }
          }
        },
        {
          name: 'clinic-map-view',
          path: '/overviewclinicmap',
          component: ViewClinicMap,
          meta: {
            breadcrumb: {
              name: 'Overview Clinic Map'
            }
          }
        },
        {
          name: 'clinic-map-upload',
          path: '/uploadclinicmap',
          component: UploadClinicMap,
          meta: {
            breadcrumb: {
              name: 'Upload Clinic Map'
            }
          }
        },
        {
          name: 'incentive-config',
          path: '/incentiveconfig',
          component: IncentiveConfig,
          meta: {
            breadcrumb: {
              name: 'Incentive Scheme Configuration'
            }
          }
        },
        {
          name: 'create-incentive',
          path: '/createincentive',
          component: CreateIncentive,
          meta: {
            breadcrumb: {
              name: 'Create Incentive Scheme'
            }
          }
        },
        {
          name: 'pre-simulation',
          path: '/presimulation',
          component: PreSimulation,
          meta: {
            breadcrumb: {
              name: 'Proses Incentive'
            }
          }
        },
        {
          name: 'pre-simulation-victory',
          path: '/presimulationvictory',
          component: PreSimulationVictory,
          meta: {
            breadcrumb: {
              name: 'Proses Incentive'
            }
          }
        },
        {
          name: 'EditScheme',
          path: '/editscheme',
          component: EditScheme,
          meta: {
            breadcrumb: {
              name: 'Edit-Scheme'
            }
          }
        },
        {
          name: 'upload-target-config',
          path: '/uploaddata',
          component: Upload,
          meta: {
            breadcrumb: {
              name: 'Upload Data'
            }
          }
        },
        {
          name: 'sales-report-config',
          path: '/salesreport',
          component: SalesReport,
          meta: {
            breadcrumb: {
              name: 'Sales Report No List'
            }
          }
        },
        {
          name: 'add-customer-regular',
          path: '/addcustomerregular',
          component: AddCustomerRegular,
          meta: {
            breadcrumb: {
              name: 'Add Customer Regular'
            }
          }
        },
        {
          name: 'overview-customer-regular-config',
          path: '/overviewcustomerregular',
          component: OverviewCustomerRegular,
          meta: {
            breadcrumb: {
              name: 'Overview Customer Regular'
            }
          }
        },
        {
          name: 'upload-customer-regular-config',
          path: '/uploadcustomerregular',
          component: UploadCustomerRegular,
          meta: {
            breadcrumb: {
              name: 'Upload Customer Regular'
            }
          }
        },
        {
          name: 'claim-sales-panel-config',
          path: '/claimsalespanel',
          component: ClaimSalesPanel,
          meta: {
            breadcrumb: {
              name: 'Claim Sales Panel'
            }
          }
        },
        {
          name: 'add-customer-contribution',
          path: '/addcustomercontribution',
          component: AddContribution,
          meta: {
            breadcrumb: {
              name: 'Add Cust. Contribution'
            }
          }
        },
        {
          name: 'overview-customer-contribution',
          path: '/overviewcustomercontribution',
          component: OverviewContribution,
          meta: {
            breadcrumb: {
              name: 'Overview Cust. Contribution'
            }
          }
        },
        {
          name: 'upload-customer-contribution',
          path: '/uploadcustomercontribution',
          component: UploadContribution,
          meta: {
            breadcrumb: {
              name: 'Upload Cust. Contribution'
            }
          }
        },
        {
          name: 'additional-data-upload',
          path: '/additional-data-upload',
          component: additionalDataUpload,
          meta: {
            breadcrumb: {
              name: 'Upload Data Pendukung'
            }
          }
        }     
      ]
    },
    { name: 'login', path: '/login', component: LoginPage }
  ]
})

export default router
