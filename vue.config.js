const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: { 
    allowedHosts: 'all',
    webSocketServer: false,
    proxy: {
      '^/api': {
        target: 'http://localhost:3310',
        changeOrigin: true,
        pathRewrite: { '^/api': '' },
        secure: true
      }
    }
  },
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/dev/'
})
// module.exports = {
//   devServer: {
//     client: {
//       logging: 'info',
//     },
//     allowedHosts: [
//       'sales-incentive.ifocusng.com'
//     ]
//   }
// }
// module.exports = defineConfig({
//   transpileDependencies: true
// })
